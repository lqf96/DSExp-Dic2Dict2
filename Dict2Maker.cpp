#include <fstream>
#include <iostream>
#include <locale>
#include <string>
#include <climits>
#include "SBT.h"
#include "String.h"
#include "Array.h"

//Debug switch
//#define EXP1_DM_DEBUG

//Exp1 dictionary maker namespace
namespace Exp1_DM
{
	//Make "dict2" core function
	Exp1::Array<SBTNode> MakeDict2(Exp1::Array<Exp1::WString> RawDict)
	{
		Exp1::Array<SBTNode> Tree;
		auto CurrentChar = L'\0';
		unsigned int i, j;
		unsigned int Root = 0;

		//Make SBT
		NodeAmount = 0;
		for (i = 0; i < RawDict.Size(); i++)
		{
			auto Word = RawDict[i];
			
			if (Word.Empty())
				continue;

			if (Word.First() != CurrentChar)
			{
				Insert(Root, Word.First());
				CurrentChar = Word.First();
			}
		}
		//Copy record to tree
		SBTNode BeginNode;
		BeginNode.LNode = BeginNode.RNode = 0;
		BeginNode.Char = L'\0';
		BeginNode.ChildRoot = Root;
		BeginNode.WordEnd = false;
		Tree.Push(BeginNode);
		for (i = 1; i <= NodeAmount; i++)
			Tree.Push(T[i]);

		//Build sub raw dict, get sub tree and merge with current tree
		auto BaseTreeSize = Tree.Size();
		for (i = 0; i < BaseTreeSize; i++)
		{
			CurrentChar = Tree[i].Char;
			Exp1::Array<Exp1::WString> SubRawDict;

			for (j = 0; j < RawDict.Size(); j++)
				if (RawDict[j].First() == CurrentChar)
				{
					if (RawDict[j].Size() == 1)
						Tree.At(i).WordEnd = true;
					else
						SubRawDict.Push(RawDict[j].SubStr(1));
				}

			if (SubRawDict.Empty())
				continue;

			auto SubTree = MakeDict2(SubRawDict);
			auto SubTreeSize = SubTree.Size();
			auto TreeSize = Tree.Size();
			for (j = 0; j < SubTreeSize; j++)
			{
				if (SubTree[j].LNode != 0)
					SubTree.At(j).LNode += TreeSize;
				if (SubTree[j].RNode != 0)
					SubTree.At(j).RNode += TreeSize;
				if (SubTree[j].ChildRoot != 0)
					SubTree.At(j).ChildRoot += TreeSize;
			}

			Tree.At(i).ChildRoot = TreeSize;
			Tree += SubTree;
		}

		return Tree;
	}
}

//Program entry
int main(int ArgC, char** ArgV)
{
	unsigned int i;

	//Set locale
	std::locale::global(std::locale(""));

#ifdef EXP1_DM_DEBUG
	//Debug path
	Exp1::String DictPath = "[Place debug path here]";
	Exp1::String Dict2Path = "[Place debug path here]";
#else
	//Not enough argument, show help
	if (ArgC < 3)
	{
		std::cout << "Usage: dict2_maker [Dictionary Path] [Disabled Words Path (Optional)] [Dict2 Dictionary Path]" << std::endl;
		std::cout << "(Please check the format of the raw dictionary to prevent the program from "
			"running into an infinite loop)" << std::endl;
		return 0;
	}

	//Get path
	Exp1::String DictPath = ArgV[1];
	bool HasDisabledWordsDict = (ArgC > 3);
	Exp1::String DisabledWordsDict = HasDisabledWordsDict ? ArgV[2] : "";
	Exp1::String Dict2Path = HasDisabledWordsDict ? ArgV[3] : ArgV[2];
#endif

	//Read dictionary from file
	std::wifstream DictFileIn(DictPath);
	if (!DictFileIn.good())
	{
		std::cout << "[Error 1] Failed to open file \"" + DictPath + "\" for reading! Aborted." << std::endl;
		return 1;
	}
	Exp1::WString RawDictContent;

	Exp1::GetAll(DictFileIn, RawDictContent);
	DictFileIn.close();

	//Read disabled words dictionary from file if exists
	Exp1::WString DWDictContent;
	if (!DisabledWordsDict.Empty())
	{
		std::wifstream DWDFileIn(DisabledWordsDict);
		if (!DWDFileIn.good())
		{
			std::cout << "[Error 1] Failed to open file \"" + DisabledWordsDict + "\" for reading! Aborted." << std::endl;
			return 1;
		}

		Exp1::GetAll(DWDFileIn, DWDictContent);
		DWDFileIn.close();
	}
	
	//Process the raw file and make raw dictionary
	auto RawDict = RawDictContent.Split(L'\n');
	if (!DisabledWordsDict.Empty())
	{
		auto DWDict = DWDictContent.Split(L'\n');
		for (i = 0; i < DWDict.Size(); i++)
		{
			unsigned int DisabledWordIndex;
			if (RawDict.Contains(DWDict[i], &DisabledWordIndex))
			{
				RawDict.Remove(DisabledWordIndex);
				continue;
			}
		}
	}
	auto SBTDict = Exp1_DM::MakeDict2(RawDict);
	
	//Open a new file
	std::ofstream Dict2FileOut(Dict2Path, std::ios::binary);
	if (!Dict2FileOut.good())
	{
		std::cout.clear();
		std::cout << "Failed to open file \"" + DictPath + "\" for writing! Aborted." << std::endl;
		return 2;
	}
	unsigned int MemorySize = SBTDict.Size() * sizeof(Exp1_DM::SBTNode);
	//Write "dict3" magic and memory size
	Dict2FileOut.write("dict3", 5);
	//Write memory
	Dict2FileOut.write((char*)(&(SBTDict.operator[](0))), MemorySize);
	Dict2FileOut.close();

	return 0;
}