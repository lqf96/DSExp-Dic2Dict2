#pragma once
#include "Array.h"

//This word matching tree utilizes SBT (Size Balanced Tree) by Chen Qifeng.
//SBT is easy to implement, supports a lot of features, and has high efficiency.
//The paper can be found at "http://zh.scribd.com/doc/3072015/10-%E9%99%88%E5%90%AF%E5%B3%B0-Size-Balanced-Tree".

//Exp1 dictionary maker namespace
namespace Exp1_DM
{
	//Temp tree storage size
	const unsigned int TSize = 50000;

	//SBT node
	struct SBTNode
	{	//Character as key
		wchar_t Char;
		//Left and right node
		unsigned int LNode;
		unsigned int RNode;
		//Child tree root position
		unsigned int ChildRoot;
		//Indicates if a word ends here
		bool WordEnd;
	};

	//Size balanced tree
	extern SBTNode T[TSize];
	//Node amount
	extern unsigned int NodeAmount;

	//SBT rotation (Left)
	void LRotate(unsigned int& x);
	//SBT rotation (Right)
	void RRotate(unsigned int& x);
	//SBT maintainance
	void Maintain(unsigned int& x, bool Flag);
	//SBT insertion
	void Insert(unsigned int& x, wchar_t NewChar);
}