#include "SBT.h"

//Exp1 dictionary maker namespace
namespace Exp1_DM
{
	//Size balanced tree
	SBTNode T[TSize];
	//SBT node size
	unsigned int NSize[TSize];
	//Node amount
	unsigned int NodeAmount;
}

//SBT rotation (Left)
void Exp1_DM::LRotate(unsigned int& x)
{
	unsigned int k = T[x].RNode;
	T[x].RNode = T[k].LNode;
	T[k].LNode = x;
	NSize[k] = NSize[x];
	NSize[x] = 1;
	NSize[x] = NSize[T[x].LNode] + NSize[T[x].RNode] + 1;
	x = k;
}

//SBT rotation (Right)
void Exp1_DM::RRotate(unsigned int& x)
{
	unsigned int k = T[x].LNode;
	T[x].LNode = T[k].RNode;
	T[k].RNode = x;
	NSize[k] = NSize[x];
	NSize[x] = NSize[T[x].LNode] + NSize[T[x].RNode] + 1;
	x = k;
}

//SBT maintainance
void Exp1_DM::Maintain(unsigned int& x, bool Flag)
{
	if (Flag)
	{
		if (NSize[T[T[x].RNode].RNode]>NSize[T[x].LNode])
			LRotate(x);
		else if (NSize[T[T[x].RNode].LNode]>NSize[T[x].LNode])
		{
			RRotate(T[x].RNode);
			LRotate(x);
		}
		else
			return;
	}
	else
	{
		if (NSize[T[T[x].LNode].LNode]>NSize[T[x].RNode])
			RRotate(x);
		else if (NSize[T[T[x].LNode].RNode]>NSize[T[x].RNode])
		{
			LRotate(T[x].LNode);
			RRotate(x);
		}
		else
			return;
	}
	
	Maintain(T[x].LNode, false);
	Maintain(T[x].RNode, true);
	Maintain(x, false);
	Maintain(x, true);
}

//SBT insertion
void Exp1_DM::Insert(unsigned int& x, wchar_t NewChar)
{
	if (x == 0)
	{
		SBTNode NewNode;
		NewNode.LNode = NewNode.RNode = NewNode.ChildRoot = 0;
		NewNode.Char = NewChar;
		NewNode.WordEnd = false;

		x = ++NodeAmount;
		T[NodeAmount] = NewNode;
		NSize[NodeAmount] = 1;
	}
	else
	{
		NSize[x]++;
		if (NewChar < T[x].Char)
			Insert(T[x].LNode, NewChar);
		else
			Insert(T[x].RNode, NewChar);
		Maintain(x, NewChar >= T[x].Char);
	}
}