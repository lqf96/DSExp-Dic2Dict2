# Flags
CXXFLAGS = -std=c++11

# Targets
dic2dict2: SBT.o Dict2Maker.o
	c++ -o dic2dict2 SBT.o Dict2Maker.o
SBT.o: SBT.h
Dict2Maker.o: Array.h SBT.h String.h

# Helper commands
clean:
	rm -f dic2dict2 *.o
