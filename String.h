#pragma once
#include <iostream>
#include "Array.h"

//Experiment 1 namespace
namespace Exp1
{
	//Basic string class
	template <typename CT>
	class BasicString : public Array<CT>
	{private:
		//Empty string
		const static CT EmptyStr;
	public:
		//Constructor
		BasicString() {}

		//Constructor (With C string)
		BasicString(const CT* RawPtr)
		{
			while (*RawPtr)
			{
				this->Push(*RawPtr);
				RawPtr++;
			}
		}

		//Constructor (from character array)
		BasicString(const Array<CT>& CharArray) : Array<CT>(CharArray) {}

		//Constructor (from number)
		BasicString(unsigned long long Number)
		{
			CT CurrentPos = Number % 10;
			while (Number != 0)
			{
				this->FPush(CurrentPos + (CT)'0');
				Number /= 10;
				CurrentPos = Number % 10;
			}
		}

		//Copy constructor
		BasicString(const BasicString<CT>& _Str) : Array<CT>(_Str) {};

		//Return C string (Read-only access)
		operator const CT*()
		{
			//Empty string
			if (this->Size() == 0)
				return &(BasicString<CT>::EmptyStr);
			
			//Get internal array reference (This is a trick!)
			const CT* __IArray = &(this->operator[](0));
			//String not ending with '\0', "append" a '\0' by doing a trick
			if ((this->Size() == this->Capacity()) || (__IArray[this->Size()] != CT()))
			{
				this->Push(CT());
				this->Pop();
			}

			//Then return internal array as a C string
			return __IArray;
		}

		//Overload operators for single character
		BasicString<CT> operator +(CT Character) const
		{
			BasicString<CT> NewStr = *this;
			NewStr.Push(Character);
			return NewStr;
		}

		//Overload operators for single character
		BasicString<CT>& operator +=(CT Character)
		{
			this->Push(Character);
			return *this;
		}

		//Overload operators for basic string
		//(By calling base class method)
		BasicString<CT>& operator =(const BasicString<CT>& _Str)
		{
			this->Array<CT>::operator=(_Str);
			return *this;
		}

		//Overload operators for basic string
		BasicString<CT> operator +(const BasicString<CT>& _Str) const
		{
			Array<CT> NewString = *this;
			NewString += _Str;
			return NewString;
		}

		//Overload operators for C string
		BasicString<CT>& operator +=(const BasicString<CT>& _Str)
		{
			this->Array<CT>::operator+=(_Str);
			return *this;
		}

		//Overload operators for C string
		BasicString<CT>& operator =(const CT* RawPtr)
		{
			*this = Exp1::BasicString<CT>(RawPtr);
			return *this;
		}

		//Overload operators for C string
		BasicString<CT>& operator +=(const CT* RawPtr)
		{
			while (*RawPtr)
			{
				this->Push(*RawPtr);
				RawPtr++;
			}
			return *this;
		}

		//Overload operators for C string
		BasicString<CT> operator +(const CT* RawPtr) const
		{
			BasicString<CT> NewStr = *this;
			while (*RawPtr)
			{
				NewStr.Push(*RawPtr);
				RawPtr++;
			}
			return NewStr;
		}

		//Compare string
		bool operator ==(const BasicString<CT>& Str2) const
		{
			unsigned int i;
			
			if (this->Size() != Str2.Size())
				return false;
			for (i = 0; i < this->Size(); i++)
				if (this->operator[](i) != Str2[i])
					return false;

			return true;
		}

		//Compare string (C string)
		bool operator ==(const CT* Str2) const
		{
			return BasicString<CT>(Str2) == *this;
		}

		//Compare string
		bool operator !=(const BasicString<CT>& Str2) const
		{
			return !(*this == Str2);
		}

		//Compare string (C string)
		bool operator !=(const CT* Str2) const
		{
			return BasicString<CT>(Str2) != *this;
		}

		//Split a string
		Array<BasicString<CT>> Split(CT SplitChar, bool IgnoreNullStr = true) const
		{
			Array<BasicString<CT>> Result;
			BasicString<CT> Tmp;
			unsigned int i;

			for (i = 0; i < this->Size(); i++)
				if (this->operator[](i) == SplitChar)
				{
					if (IgnoreNullStr || (!Tmp.Empty()))
						Result.Push(Tmp);
					Tmp = BasicString<CT>();
				}
				else
					Tmp += this->operator[](i);
			if (IgnoreNullStr || (!Tmp.Empty()))
				Result.Push(Tmp);
			
			return Result;
		}

		//Get a sub-string from the string
		BasicString<CT> SubStr(unsigned int Begin = 0) const
		{
			return this->SubArray(Begin);
		}

		//Get a sub-string from the string
		BasicString<CT> SubStr(unsigned int Begin, unsigned int End) const
		{
			return this->SubArray(Begin, End);
		}

		//Replace a character with another
		BasicString<CT> Replace(CT OldChar, CT NewChar) const
		{
			BasicString<CT> NewStr;
			unsigned int i;

			for (i = 0; i < this->Size(); i++)
				NewStr += (this->operator[](i) == OldChar) ? NewChar : this->operator[](i);
			return NewStr;
		}

		//Remove a specific character from the string
		BasicString<CT> Replace(CT RemoveChar) const
		{
			BasicString<CT> NewStr;
			unsigned int i;

			for (i = 0; i < this->Size(); i++)
			{
				if (this->operator[](i) != RemoveChar)
					NewStr += this->operator[](i);
			}
			return NewStr;
		}
	};

	//Empty string constant
	template <typename CT>
	const CT BasicString<CT>::EmptyStr = CT();

	//Overload operators for C string
	template <typename CT>
	BasicString<CT> operator +(const CT* RawPtr, const BasicString<CT>& Str)
	{
		BasicString<CT> NewStr = Str;
		while (*RawPtr)
		{
			NewStr.Push(*RawPtr);
			RawPtr++;
		}
		NewStr += Str;
		return NewStr;
	}

	//Overload operators for single character
	template <typename CT>
	BasicString<CT> operator +(CT Character, const BasicString<CT>& Str)
	{
		BasicString<CT> NewStr = Str;
		NewStr.FPush(Character);
		return NewStr;
	}

	//Output string
	template <typename CT>
	std::basic_ostream<CT>& operator <<(std::basic_ostream<CT>& Out, const BasicString<CT>& Str)
	{
		unsigned int i;
		for (i = 0; i < Str.Size(); i++)
			Out << Str[i];
		return Out;
	}

	//Input string
	template <typename CT>
	std::basic_istream<CT>& operator >>(std::basic_istream<CT>& In, BasicString<CT>& Str)
	{
		CT __Character = In.get();
		while ((__Character != (CT)' ') && (!In.eof()))
		{
			Str.Push(__Character);
			__Character = In.get();
		}
		return In;
	}

	//Input a whole line into a string
	template <typename CT>
	std::basic_istream<CT>& GetLine(std::basic_istream<CT>& In, BasicString<CT>& Str)
	{
		CT __Character = In.get();
		while ((__Character != (CT)'\n') && (!In.eof()))
		{
			if (__Character != (CT)'\r')
				Str.Push(__Character);
			__Character = In.get();
		}
		return In;
	}

	//Read all content until EOF
	template <typename CT>
	std::basic_istream<CT>& GetAll(std::basic_istream<CT>& In, BasicString<CT>& Str)
	{
		while (!In.eof())
			Str.Push(In.get());
		return In;
	}

	//Standard string
	typedef BasicString<char> String;
	//Wide character string
	typedef BasicString<wchar_t> WString;
}