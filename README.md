# 数据结构与算法(1) 第二阶段课程实验 字典转换工具

## 用法
```bash
dic2dict2 [Dictionary Path] [Disabled Words Path (Optional)] [Dict2 Dictionary Path]
```

## 说明
本程序用于将一顺序存储词条的词典转换为基于Trie树的字典格式。
使用前，请务必检查词典文件是否有EOF（否则可能导致程序陷入死循环），词典文件中的词条是否顺序存储。
注意此程序产生的词典文件与第一阶段课程实验中早期版本产生的词典不兼容，不得混用。

## 许可
本项目基于GNU GPLv3发布。
